# ezezaguna's blog theme

[Hugo](https://gohugo.io) theme for my website at [ezezaguna.poo.li](https://ezezaguna.poo.li)

## License

MIT

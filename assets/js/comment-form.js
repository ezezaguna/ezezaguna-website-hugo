function f() { // form focus
	if (g('comf-na').value) {
		g('comf-co').focus();
	} else {
		g('comf-na').focus();
	}
}
function re(th,l,name) { // reply to a comment
	/* set form fields */
	g('comf-ith').value = th.title;
	g('comf-ina').value = name;
	g('comf-iid').value = th.id;

	/* replace text */
	t(g('comf-n'), name);
	t(g('comf-rt'), 'you are replying to');

	g('comf-l').href = '#comment-' + l;
	g('comf-r').classList.remove('invisible');
	g('comf-r').classList.add('visible');
	var notScTim = setTimeout(f, 200)
	addEventListener('scroll', function(e) {
		clearTimeout(notScTim);
		removeEventListener('scroll', arguments.callee);
		scTim = setTimeout(function() {
		    f();
		}, 110);
	});
}
function rer() {
	g('comf-ith').value = '';
	g('comf-ina').value = '';
	g('comf-iid').value = '';
	g('comf-r').classList.remove('visible');
	g('comf-r').classList.add('invisible');
	f();
}

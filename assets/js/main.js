/* shorthand for a smaller js file */
function g(i) {
	return document.getElementById(i);
}
/* replace inner text function */
function t(e,t) { // 1 letter var names are bad but i want M AXIM UM C O MPR ES SION
	var p = ('textContent' in e) ? 'textContent' : 'innerText';
	e[p] = t;
}
window.onload = function() {
	const elNav = g('site-nav').children[0];
	const Nav = {
		elTop: g('top'),
		opt: {
			root: null,
			rootMargin: '0px',
			threshold: 0
		},
		obs: null,
		hndl(entries, observer) {
			if (entries[0].isIntersecting) elNav.classList.remove('scroll');
			else elNav.classList.add('scroll');
		},
		init() {
			this.obs = new IntersectionObserver(this.hndl, this.opt);
			this.obs.observe(this.elTop);
		}
	};
	Nav.init();
};
